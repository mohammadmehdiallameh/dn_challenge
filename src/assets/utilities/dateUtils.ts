export const getTimeInterval = (date: number) => { // Get Unix Time and Compare With Now
    let seconds = Math.floor((Date.now() - date) / 1000);
    let unit = "s";
    let direction = "ago";
    if (seconds < 0) {
        seconds = -seconds;
        direction = "from now";
    }
    let value = seconds;
    if (seconds >= 31536000) {
        value = Math.floor(seconds / 31536000);
        unit = "y";
    } else if (seconds >= 86400) {
        value = Math.floor(seconds / 86400);
        unit = "d";
    } else if (seconds >= 3600) {
        value = Math.floor(seconds / 3600);
        unit = "h";
    } else if (seconds >= 60) {
        value = Math.floor(seconds / 60);
        unit = "m";
    }
    if (value <= 5) {
        return unit = 'a few seconds ago'
    }
    return value + unit + " " + direction;

}